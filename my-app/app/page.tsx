'use client'

import Image from "next/image";
import Spline from '@splinetool/react-spline';

//    <Spline scene="https://prod.spline.design/3Te1aLsru3S4Jllb/scene.splinecode" />
//    <Spline scene="https://prod.spline.design/3Te1aLsru3S4Jllb/scene.splinecode" />
export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <Spline scene="https://prod.spline.design/3Te1aLsru3S4Jllb/scene.splinecode" />
    </main>
  );
}
